import React, {useEffect, useState} from 'react';
import AxiosApi from "../../axiosApi";
import {useParams} from "react-router-dom";

const ContentContainer = () => {
    const [page, setPage] = useState({});
    const params = useParams();

    const getPage = async () => {
        const {data} = await AxiosApi.get(`/pages/${params.page}.json`);
        setPage(data)
    }

    useEffect(() => {
        getPage();
    }, [params.page])

    return (
        page
        ? <div className="container">
                <h3 className="text-capitalize">{page.title}</h3>
                <div className="card p-2">
                    <p>{page.content}</p>
                </div>
            </div> : null
    );
};

export default ContentContainer;