import React, {useEffect, useState} from 'react';
import AxiosApi from "../../axiosApi";
import {useHistory} from "react-router-dom";

const AdminPage = () => {
    const [pages, setPages] = useState();
    const [value, setValue] = useState({
        title: '',
        content: ''
    });
    const [page, setPage] = useState('');
    const history = useHistory();

    const getPages = async () => {
        const {data} = await AxiosApi.get('/pages.json');
        setPages(data);
    }

    const putAxios = async (e) => {
        e.preventDefault();
        const newData = {
            title: value.title,
            content: value.content
        }

        await AxiosApi.put(`./pages/${page}.json`, newData);
        history.push('/')

    }

    const selectChange = e => {
        const {value} = e.target;
        setPage(value);
    }

    const inputChange = e => {
        const {name, value} = e.target;
        setValue(prev => ({
            ...prev,
            [name]: value
        }));
    }

    useEffect(() => {
        getPages();
    }, [])

    useEffect(() => {
        if (page && pages) {
            setValue(pages[page])
        }
    }, [page, pages])

    return (
        pages
        ? <div className="container">
                <h3>Edit page</h3>
                <form onSubmit={putAxios}>
                    <div className="input-group my-2">
                        <div className="input-group-prepend">
                            <label className="input-group-text">Select page</label>
                        </div>
                        <select
                            className="form-select"
                            aria-label="Default select example"
                            onChange={selectChange}
                        >
                            <option>Open this select menu</option>
                            {pages ? Object.keys(pages).map(page => (
                                <option key={page} value={page}>{page}</option>
                            )) : null}
                        </select>
                    </div>
                    <div className="form-group my-2">
                        <label>Title</label>
                        <input
                            className="form-control"
                            name="title"
                            value={value.title}
                            onChange={inputChange}
                            type="text"
                            placeholder="Change title"
                        />
                    </div>
                    <div className="form-group my-2">
                        <label>Content</label>
                        <textarea
                            className="form-control"
                            style={{resize: "none", height: "150px"}}
                            name="content"
                            onChange={inputChange}
                            value={value.content}
                            placeholder="Change content"
                        />
                    </div>
                    <div className="form-group">
                        <button
                            className="btn btn-primary"
                        >
                            Save
                        </button>
                    </div>
                </form>
            </div> : null
    );
};

export default AdminPage;