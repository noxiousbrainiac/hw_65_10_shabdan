import React from 'react';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import Layout from "./components/UI/Layout/Layout";
import AdminPage from "./containers/AdminPage/AdminPage";
import ContentContainer from "./containers/ContentContainer/ContentContainer";

const App = () => {
    return (
        <BrowserRouter>
            <Layout>
                <Switch>
                    <Route
                        path="/"
                        exact
                        render={() => (
                                <div className="container">
                                    <h4>Hello , it,s Home page</h4>
                                </div>
                        )}
                    />
                    <Route path="/admin" component={AdminPage}/>
                    <Route path="/:page" component={ContentContainer}/>
                </Switch>
            </Layout>
        </BrowserRouter>
    );
};

export default App;