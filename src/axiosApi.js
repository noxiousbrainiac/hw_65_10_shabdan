import axios from "axios";

const AxiosApi = new axios.create({
    baseURL: "https://layout-app-forhw65-default-rtdb.europe-west1.firebasedatabase.app"
})

export default AxiosApi;