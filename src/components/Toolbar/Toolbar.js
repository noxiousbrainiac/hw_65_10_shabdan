import React from 'react';
import {Link} from "react-router-dom";

const Toolbar = () => {
    const pages = [
        {title: "about"},
        {title: "contacts"},
        {title: "download"},
        {title: "media"},
        {title: "rules"}
    ]

    return (
        <header
            className="bg-light d-flex justify-content-between align-items-center"
            style={{width: "100%", padding: "0 20px", boxSizing: "border-box"}}
        >
            <h2>Home Page</h2>
            <nav
                className="navbar navbar-expand-lg"
                style={{height: "100%"}}
            >
                <ul className="navbar-nav mr-auto">
                    <li className="nav-item">
                        <Link to="/" className="nav-link">
                            <b>Home</b>
                        </Link>
                    </li>

                    {pages.map(page => (
                        <li key={page.title} className="nav-item">
                            <Link to={`/${page.title}`} className="nav-link text-capitalize">{page.title}</Link>
                        </li>
                    ))}

                    <li className="nav-item">
                        <Link to="/admin" className="nav-link">
                            <b>Admin</b>
                        </Link>
                    </li>
                </ul>
            </nav>
        </header>
    );
};

export default Toolbar;